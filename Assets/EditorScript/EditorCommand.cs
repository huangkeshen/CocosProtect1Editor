﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LitJson;
using UnityEditorInternal;
using System;
using System.IO;

public class ImageInporter : AssetPostprocessor {
    void OnPreprocessTexture() {
        TextureImporter textureimport = (TextureImporter)base.assetImporter;
        if (assetPath.StartsWith("Asset/Resources/Texture/")) {
            textureimport.textureType = TextureImporterType.Sprite;
        }
    }
}

public class EditorCommand : EditorWindow
{
    private string ConfigurationPath = "Configuration/";
    private string ObjectPath = "Object/";
    
    [SerializeField]
    private UnityEngine.Object BlockTransForm;
    private GameObject RootGameobject;
    public List<JsonData> BlockCollection = new List<JsonData>();
    public List<FileInfo> FilesInfoCollection = new List<FileInfo>();
    private ReorderableList _list;
    public int select_index;
    private FileInfo CurrentUsageFile; 
    private List<BrickInfo> BrickList = new List<BrickInfo>();
    private Dictionary<string, object> jsonMainDic = new Dictionary<string, object>();


    [MenuItem("Edit/EditBlockInfo")]
    static void ShowWindow() {
        Rect window_rect = new Rect(0,0,500,1000);
        EditorCommand command = (EditorCommand)EditorWindow.GetWindowWithRect(typeof(EditorCommand),window_rect,true,"BlockEdit");
        command.Show();
    }
    private void OnEnable()
    {
        BlockCollection.Clear();
        string path =Application.dataPath+ "/Resources/RunningResource/Configuration";
        if (Directory.Exists(path)) {
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            FileInfo[] filesInfo = dirInfo.GetFiles("*",SearchOption.AllDirectories);
            Debug.LogError("Searching Files!!!.......");
            if (filesInfo.Length > 0) {
                foreach (FileInfo file in filesInfo)
                {
                    if (file.Name.EndsWith(".json")&&!file.Name.StartsWith("Config"))
                    {
                        FilesInfoCollection.Add(file);
                        Debug.LogError(file.Name);
                    }
                }
            }
        }
        _list = new ReorderableList(FilesInfoCollection, FilesInfoCollection.GetType());
        _list.drawElementCallback = (rect, select_index, isActive, isFocused) =>
        {
            EditorGUI.TextField(new Rect(rect.x, rect.y, 300, 20),FilesInfoCollection[select_index].Name);
        };
        _list.onSelectCallback = (list) =>
        {
            select_index = list.index;
            CurrentUsageFile = FilesInfoCollection[select_index];
        };
        _list.onRemoveCallback = (list) => {
            File.Delete(path+"//"+FilesInfoCollection[select_index].FullName);
            FilesInfoCollection.Remove(FilesInfoCollection[select_index]);
            CreatOrCoverConfigurationFile();
        };
        BlockTransForm = GameObject.Find("Block")as UnityEngine.Object;
    }
    private string FileName;
    private string FileName2;
    //delegate void SetNames(string s1, string s2);
    //SetNames setNames;
    List<BrickInfo> infoList;
    private void OnGUI()
    {
        infoList = new List<BrickInfo>();
        infoList.Sort((x,y)=>x.pos_x.CompareTo(y.pos_x));
         //setNames = (s1, s2) => { FileName = s1; FileName2 = s2; };
        //setNames("Road1","Road2");
        GUILayout.BeginVertical();
        _list.DoLayoutList();
        BlockTransForm = EditorGUILayout.ObjectField("BlockRoot",BlockTransForm, typeof(object));
        RootGameobject = BlockTransForm as GameObject;
        FileName = EditorGUILayout.TextField("文件名:",FileName);
        if (GUILayout.Button("Creat")) {
            StreamWriter sw=null;
            FileInfo fileinfo = new FileInfo(Application.dataPath + "/Resources/RunningResource/Configuration/" + FileName + ".json");
            if (!fileinfo.Exists) {
                sw = fileinfo.CreateText();
            }
            sw.Close();
            sw.Dispose();
            FilesInfoCollection.Add(fileinfo);
            CurrentUsageFile = fileinfo;
            CreatOrCoverConfigurationFile();
        }
        if (GUILayout.Button("Save")) {
            WriteJsonIntoCurrentFile(CurrentUsageFile);
            CreatOrCoverConfigurationFile();
        }
        if (GUILayout.Button("Load"))
        {
            CurrentUsageFile = FilesInfoCollection[select_index];
            LoadJsonFile(CurrentUsageFile);
        }
    }
    private void LoadJsonFile(FileInfo file) {
        FileStream fs = file.OpenRead();
        byte[] buffer = new byte[fs.Length];
        int r = fs.Read(buffer,0,buffer.Length);
        string str = System.Text.Encoding.UTF8.GetString(buffer,0,r);
        JsonData json = JsonMapper.ToObject(str);
        JsonData node = json["Infos"];
        if (RootGameobject.transform.childCount >= 1) {
            for (int index = RootGameobject.transform.childCount-1; index >= 0; index--)
            {
                DestroyImmediate(RootGameobject.transform.GetChild(index).gameObject,true);
            }
        }
        for (int i = 0; i < node.Count; i++)
        {
            UnityEngine.Object obj = Resources.Load("Object/" + node[i]["name"]);
            GameObject gobj = Instantiate(obj, RootGameobject.transform) as GameObject;
            float pos_x = float.Parse(node[i]["pos_x"].ToString());
            float pos_y = float.Parse(node[i]["pos_y"].ToString()) - 149f;
            float size_x = float.Parse(node[i]["size_x"].ToString());
            float size_y = float.Parse(node[i]["size_y"].ToString());
            gobj.transform.localPosition = new Vector3(pos_x, pos_y, 0);
            gobj.GetComponent<RectTransform>().sizeDelta = new Vector2(size_x, size_y);
            gobj.name = node[i]["name"].ToString();
        }

        fs.Close();
        fs.Dispose();
    }
    private void CreatOrCoverConfigurationFile() {
        List<Dictionary<string, object>> ConfigList = new List<Dictionary<string, object>>();
        
        foreach (FileInfo file in FilesInfoCollection) {
            Dictionary<string, object> fileInfoDic = new Dictionary<string, object>();
            string[] strs = file.Name.Split('-');
            string[] speedStr = strs[2].Split('.');
            fileInfoDic.Add(file.Name, speedStr[0]);
            ConfigList.Add(fileInfoDic);
        }
        Dictionary<string, object> ConfigInfo = new Dictionary<string, object>();
        ConfigInfo.Add("Config",ConfigList);
        string ConfigStr = JsonMapper.ToJson(ConfigInfo);
        FileStream fs = File.Open(Application.dataPath+ "/Resources/RunningResource/Configuration/Configurations.json", FileMode.Create);
        byte[] content = System.Text.Encoding.UTF8.GetBytes(ConfigStr);
        fs.Write(content,0,content.Length);
        fs.Close();
        fs.Dispose();
    }
    private void WriteJsonIntoCurrentFile(FileInfo CurrentInfo) {
        Debug.LogError(RootGameobject.transform.childCount);
        List<object> brickinfoList = new List<object>();
        Dictionary<string, object> BlockInfos = new Dictionary<string, object>();
        for (int i = 0; i < RootGameobject.transform.childCount; i++) {
            Dictionary<string, string> brickInfo = new Dictionary<string, string>();
            Transform brick = RootGameobject.transform.GetChild(i);
            brickInfo.Add("name",brick.name);
            brickInfo.Add("pos_x",brick.localPosition.x.ToString());
            brickInfo.Add("pos_y",(brick.localPosition.y+149f).ToString());
            brickInfo.Add("size_x", brick.GetComponent<RectTransform>().sizeDelta.x.ToString());
            brickInfo.Add("size_y", brick.GetComponent<RectTransform>().sizeDelta.y.ToString());

            brickinfoList.Add(brickInfo);

        }
        BlockInfos.Add("Infos",brickinfoList);
        string str = JsonMapper.ToJson(BlockInfos);
        FileStream fs = File.Open(CurrentUsageFile.FullName,FileMode.OpenOrCreate);
        byte[] content = System.Text.Encoding.UTF8.GetBytes(str);
        fs.Write(content,0,content.Length);
        fs.Close();
        fs.Dispose();
        
    }
    private GameObject getPrefab(string prefabName)
    {
        UnityEngine.Object loadObject = Resources.Load(ObjectPath + prefabName);
        GameObject BlockObject = Instantiate(loadObject, RootGameobject.transform) as GameObject;
        return BlockObject;
    }
}
public class BrickInfo {
    public string name;
    public float pos_x;
    public float pos_y;
    public float size_x;
    public float size_y;
    
}
